# README #

Petunjuk untuk integrasi login g plus untuk android

### Persiapan ###

* Baca dulu dan Create Project Di [Google Developer Console](https://developers.google.com/+/mobile/android/getting-started)
* Download file [GooglePlusActivity.java](GooglePlusActivity.java)


### Cara instalasi ###

- Ubah package di file [GooglePlusActivity.java](GooglePlusActivity.java) sesuai dengan package aplikasimu
- Di manifest aplikasi tambahkan permission
```
#!xml
	<uses-permission android:name="android.permission.INTERNET" /> 
	<uses-permission android:name="android.permission.GET_ACCOUNTS" />
	<uses-permission android:name="android.permission.USE_CREDENTIALS" />
```

- Di manifest aplikasi tambahkan metadata di dalam tag <application>
```
#!xml
	<meta-data 
	    android:name="com.google.android.gms.version"
	    android:value="@integer/google_play_services_version" />
```
- Tambahkan script pada gradle di dalam tag dependencies app mu
```        
	compile 'com.google.android.gms:play-services:+'
```
- Di activitymu extends kan class GooglePlusActivity
- Di activitymu di oncreate tambahkan script
```
#!java
	setOnConnectedGPlus(new OnConnectedGPlus() {
	    @Override
	    public void onConnected(Person person, String email) {
	        //disini sudah mendapatkan data user
	    }  
	});
```
- Lalu buat button untuk login pake gplus, dan tambahkan listener ketika button tersebut di klik dengan memanggil method
```
#!java
	signInWithGplus();
```
- Enjoy

