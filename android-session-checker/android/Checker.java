package id.co.firzil.cobalibrary;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;

/**
 * Created by fahriyalafif on 7/8/2015.
 */
public class Checker {
    private Context c;
    private Dialog dialog;
    private SessionChecker svc;
    private String url_session_checker = "";
    private OnCloseClickListener onCloseClick;

    public interface OnCloseClickListener{
        void onClose();
    }

    public Checker(Context c){
        this.c = c;
        svc = new SessionChecker(c);
    }

    public void setOnCloseClickListener(OnCloseClickListener onCloseClick){
        this.onCloseClick = onCloseClick;
    }

    public void setUrlApiSessionChecker(String url_session_checker){
        this.url_session_checker = url_session_checker;
    }

    private int dpToPixel(int dp) {
        float scale = c.getResources().getDisplayMetrics().density;
        return (int) (dp * scale);
    }

    private void showDialogOffApp(){
        try{
            if(dialog == null){
                dialog = new Dialog(c);

                LinearLayout l = new LinearLayout(c);
                l.setBackgroundColor(Color.WHITE);
                l.setOrientation(LinearLayout.VERTICAL);

                int padding = dpToPixel(10);

                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                l.setLayoutParams(lp);
                l.setPadding(padding, padding, padding, padding);

                TextView judul = new TextView(c);
                judul.setText("Session Checker");
                judul.setTextColor(Color.parseColor("#511f1f"));
                judul.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                judul.setGravity(Gravity.CENTER);
                judul.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);

                l.addView(judul);

                LinearLayout.LayoutParams hm = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                hm.setMargins(0, padding, 0, padding);

                TextView msg = new TextView(c);
                msg.setText(svc.getSessionMessage());
                msg.setTextColor(Color.BLACK);
                msg.setLayoutParams(hm);
                msg.setGravity(Gravity.CENTER);
                msg.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);

                l.addView(msg);

                LinearLayout.LayoutParams button_param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                button_param.gravity = Gravity.CENTER_HORIZONTAL;

                Button ok = new Button(c);
                ok.setText("Close");
                ok.setTextColor(Color.WHITE);
                ok.setBackgroundColor(Color.parseColor("#3bafda"));
                ok.setLayoutParams(button_param);
                ok.setGravity(Gravity.CENTER);
                ok.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismissDialog();
                        if(onCloseClick != null) onCloseClick.onClose();
                    }
                });

                l.addView(ok);

                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(l);
                dialog.setCanceledOnTouchOutside(false);
                dialog.setCancelable(false);

                dialog.setTitle("");
            }

            if(! dialog.isShowing())dialog.show();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void cekSession(){
        try {
            if(svc.isBelumNgecekSession() && isOnLine() && svc.isSudah30menit()) new SessionChecking().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            else if(! svc.isSessionStatusOn()) showDialogOffApp();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void dismissDialog(){
        if(dialog != null && dialog.isShowing()) dialog.dismiss();
    }

    private boolean isOnLine(){
        try{
            ConnectivityManager connectivity = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null){
                NetworkInfo[] info = connectivity.getAllNetworkInfo();
                if (info != null){
                    for (int i = 0; i < info.length; i++){
                        if (info[i].getState() == NetworkInfo.State.CONNECTED)
                        {
                            return true;
                        }
                    }
                }
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }

        return false;
    }

    private class SessionChecking extends AsyncTask<String, String, String>{

        private JSONObject j;

        protected void onPreExecute(){
            super.onPreExecute();
            j = null;
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                URL url = new URL(url_session_checker);

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.connect();

                int resCode = conn.getResponseCode();
                InputStream in = null;
                if (resCode == HttpURLConnection.HTTP_OK) {
                    in = conn.getInputStream();
                }

                BufferedReader reader = new BufferedReader(new InputStreamReader(in, "utf-8"), 8);
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                in.close();

                String json = sb.toString();
                Log.d(" aaa ", "aaa = "+json);
                j = new JSONObject(json);

            }
            catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String x){
            super.onPostExecute(x);
            if (j != null) {
                try {
                    if (j.optInt("status_code") == 1) {
                        svc.setLastTimeCheck(new Date().getTime());

                        String session_status = j.optString("session_status");

                        svc.setSessionStatus(session_status);
                        svc.setSessionMessage(j.optString("session_message"));
                        svc.setIsSudahNgeCekSession(true);

                        if (! svc.isSessionStatusOn()) showDialogOffApp();
                    }
                }
                catch(Exception e){
                    e.printStackTrace();
                }
            }
        }

    }

}