# README #

Ini adalah library untuk mentrack app yg sedang berjalan setiap 20 detik sekali, lalu dikirim ke server app apa yg sedang berjalan

### Persiapan ###

* Download file library untuk [android](android)
* Download file [contoh](example) agar mudah memahami


### Cara instalasi ###

- Ubah package di file [android](android) sesuai dengan package aplikasimu
- Di manifest aplikasi tambahkan permission
```
#!xml
	<uses-permission android:name="android.permission.INTERNET" /> 
	<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
	<uses-permission android:name="android.permission.ACCESS_WIFI_STATE"/>
	<uses-permission android:name="android.permission.GET_TASKS" />
```

- lalu tambahkan permission di dalam tag application

```
#!xml
	<receiver android:name=".NetworkChangeReceiver" >
        <intent-filter>
            <action android:name="android.net.conn.CONNECTIVITY_CHANGE"/>
        </intent-filter>
    </receiver>

    <service android:name=".TrackService" />
```

- Di activitymu yg awal (misal splash screen)di oncreate tambahkan script

```
#!java
	startService(new Intent(this, TrackService.class));	
```

- Have Fun