package id.co.firzil.cobalibrary;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;

public class NetworkChangeReceiver extends BroadcastReceiver {

	public static boolean isOnLine(Context _context){
		try{
			ConnectivityManager connectivity = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
			if (connectivity != null){
				NetworkInfo[] info = connectivity.getAllNetworkInfo();
				if (info != null){
					for (int i = 0; i < info.length; i++){
						if (info[i].getState() == NetworkInfo.State.CONNECTED){
							return true;
						}
					}
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}

		return false;
	}

	@Override
	public void onReceive(Context c, Intent intent) {
		Intent intentScreen = new Intent(c, TrackService.class);
		if (isOnLine(c))c.startService(intentScreen);
		else c.stopService(intentScreen);
	}

}
