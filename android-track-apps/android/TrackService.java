package id.co.firzil.cobalibrary;

import android.app.ActivityManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class TrackService extends Service {
	public static final int TIME = 20;
	public static final TimeUnit TIME_UNIT = TimeUnit.SECONDS;

	private boolean on = true;
	private BroadcastReceiver mReceiver = null;
	private ScheduledExecutorService scheduleTaskExecutor;
	private Runnable track = new Runnable() {
		public void run() {
			try {
				final Context cc = getBaseContext();
				ActivityManager am = (ActivityManager) cc.getSystemService(ACTIVITY_SERVICE);
				ComponentName componentInfo = am.getRunningTasks(1).get(0).topActivity;
				String packageNow = componentInfo.getPackageName();

				PackageManager pm = cc.getPackageManager();
				CharSequence c = pm.getApplicationLabel(pm.getApplicationInfo(packageNow, PackageManager.GET_META_DATA));
				String openApp = c.toString();

				if(NetworkChangeReceiver.isOnLine(cc)){
					HashMap<String, String> postParam = new HashMap<>();
					//postParam.put("key", "");   //ini diganti param id user bisa
					postParam.put("app_name", openApp);
					postParam.put("app_status", on ? "Open" : "Close");

					String url_api = "http://xrozz.com/mlogg/api/log/post_log";  //ini diganti url api nya
					String response = performPostCall(url_api, postParam);
					Log.d("respon kirim", "respon kirim = "+response);
				}

				if(! on) {
					scheduleTaskExecutor.shutdown();
				}
			}
			catch (SecurityException s){
				s.printStackTrace();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	};
	
	@Override
	public void onCreate() {
		super.onCreate();
		IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
		filter.addAction(Intent.ACTION_SCREEN_OFF);
		mReceiver = new AEScreenOnOffReceiver();
		registerReceiver(mReceiver, filter);
		
		scheduleTaskExecutor = Executors.newScheduledThreadPool(5);
		scheduleTaskExecutor.scheduleAtFixedRate(track, 0, TIME, TIME_UNIT);
	}
	
	public void onDestroy(){
		super.onDestroy();
		unregisterReceiver(mReceiver);
		if(scheduleTaskExecutor != null) scheduleTaskExecutor.shutdown();
	}
	
	private class AEScreenOnOffReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
				on = false;
			} 
			else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
				on = true;
				if(scheduleTaskExecutor.isShutdown() || scheduleTaskExecutor.isTerminated()){
					scheduleTaskExecutor = Executors.newScheduledThreadPool(5);
					scheduleTaskExecutor.scheduleAtFixedRate(track, 0, TIME, TIME_UNIT);
				}
			}
		}

	}

	public String performPostCall(String requestURL, HashMap<String, String> postDataParams) {
		String response = "";
		try {
			URL url = new URL(requestURL);

			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setReadTimeout(15000);
			conn.setConnectTimeout(15000);
			conn.setRequestMethod("POST");
			conn.setDoInput(true);
			conn.setDoOutput(true);

			OutputStream os = conn.getOutputStream();
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
			writer.write(getPostDataString(postDataParams));

			writer.flush();
			writer.close();
			os.close();
			int responseCode = conn.getResponseCode();

			if (responseCode == HttpURLConnection.HTTP_OK) {
				String line;
				BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				while ((line=br.readLine()) != null) {
					response += line;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;
	}

	private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException{
		StringBuilder result = new StringBuilder();
		boolean first = true;
		for(Map.Entry<String, String> entry : params.entrySet()){
			if (first)
				first = false;
			else
				result.append("&");

			result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
			result.append("=");
			result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
		}

		return result.toString();
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

}