# README #

Ini adalah kumpulan library yang digunakan untuk ndevelop app:

- [Android Login Using Google Plus](android-gplus-login/gplus-login.md)
- [Android Version Checker using PHP](android-version-checker/version-checker.md)
- [Android Session Checker using PHP](android-session-checker/session-checker.md)
- [Android Track Apps](android-track-apps/track-apps.md)
- [Android Ambil Gambar](android-ambil-gambar/ambil-gambar.md)
- [Android Apps Controller](android-apps-controller/apps-controller.md)
- [Android Force Close Handler](android-force-close-handler/force-close-handler.md)
- [Android Layout Login Using Email And Password](android-login-email-pass-layout/login-email-pass-layout.md)