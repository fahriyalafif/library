package id.co.firzil.cobalibrary;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import java.io.File;

public class MainActivity extends Activity {

    private View ambil_gambar;
    private ImageView gambar;
    private AmbilGambar ag;
    private String folder_gambar;
    private Context c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        c = this;

        String app_name = getResources().getString(R.string.app_name);  //nama aplikasi, tempat menyimpan gambar
        File dir;
        if(android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
            dir = new File(Environment.getExternalStorageDirectory(), app_name);
        else
            dir = getDir(app_name, Context.MODE_PRIVATE);

        if(! dir.exists()) dir.mkdirs();

        folder_gambar = dir.getPath() + "/";

        ambil_gambar = findViewById(R.id.ambil_gambar);
        gambar = (ImageView) findViewById(R.id.gambar);

        ambil_gambar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ag == null) {
                    ag = new AmbilGambar(c, folder_gambar); //kalo manggilnya di activity
                    //ag = new AmbilGambar(c, path_direktory, fragment);  //kalo manggilnya di fragment
                }
                ag.showDialogChooser();
            }
        });

    }

    @Override
    public void onActivityResult(int request, int result, Intent data) {
        super.onActivityResult(request, result, data);
        if(result == Activity.RESULT_OK){
            if(request == AmbilGambar.PICK_FROM_CAMERA || request == AmbilGambar.PICK_FROM_FILE){
                try{
                    String path_asli = ag.processResultAndReturnImagePath(request, data);
                    gambar.setImageURI(Uri.parse(path_asli));

                    //biar bisa dibaca oleh app galery kalo ada gambar baru
                    Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse(folder_gambar));
                    sendBroadcast(intent);
                }
                catch(OutOfMemoryError o){
                    o.printStackTrace();
                }
                catch(NullPointerException o){
                    o.printStackTrace();
                }
                catch(Exception o){
                    o.printStackTrace();
                }
            }
        }
    }

}