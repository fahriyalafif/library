package id.co.firzil.cobalibrary;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by fahriyalafif on 7/8/2015.
 */
public class Updater {
    private Context c;
    private Dialog dialog;
    private SessionVersionChecker svc;
    private String url_version_checker = "";

    public Updater(Context c){
        this.c = c;
        svc = new SessionVersionChecker(c);
    }

    public void setUrlApiVersionChecker(String url_version_checker){
        this.url_version_checker = url_version_checker;
    }

    private int dpToPixel(int dp) {
        float scale = c.getResources().getDisplayMetrics().density;
        return (int) (dp * scale);
    }

    private void showDialogUpdate(){
        try{
            if(dialog == null){
                dialog = new Dialog(c);

                LinearLayout l = new LinearLayout(c);
                l.setOrientation(LinearLayout.VERTICAL);
				l.setBackgroundColor(Color.WHITE);

                int padding = dpToPixel(10);

                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                l.setLayoutParams(lp);
                l.setPadding(padding, padding, padding, padding);

                TextView judul = new TextView(c);
                judul.setText("Version Checker");
                judul.setTextColor(Color.parseColor("#511f1f"));
                judul.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                judul.setGravity(Gravity.CENTER);
                judul.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);

                l.addView(judul);

                LinearLayout.LayoutParams hm = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                hm.setMargins(0, padding, 0, padding);

                TextView msg = new TextView(c);
                msg.setText("Update version has been released. Update now!");
                msg.setTextColor(Color.BLACK);
                msg.setLayoutParams(hm);
                msg.setGravity(Gravity.CENTER);
                msg.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);

                l.addView(msg);

                LinearLayout.LayoutParams button_param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                button_param.gravity = Gravity.CENTER_HORIZONTAL;

                Button ok = new Button(c);
                ok.setText("Update");
                ok.setTextColor(Color.WHITE);
                ok.setBackgroundColor(Color.parseColor("#3bafda"));
                ok.setLayoutParams(button_param);
                ok.setGravity(Gravity.CENTER);
                ok.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String url = svc.getUrlUpdate();
                        if (url.equalsIgnoreCase(SessionVersionChecker.PLAYSTORE)) {
                            final String appPackageName = c.getPackageName();
                            try {
                                c.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                dismissDialog();
                            } catch (Exception e) {
                                e.printStackTrace();
                                Toast.makeText(c, "No Play Store Application Installed", Toast.LENGTH_LONG).show();
                                loadUrlApk("http://play.google.com/store/apps/details?id=" + appPackageName);
                            }
                        }
                        else loadUrlApk(url);
                    }
                });

                l.addView(ok);

                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(l);
                dialog.setCanceledOnTouchOutside(false);

                dialog.setTitle("");
            }
            dialog.setCancelable(svc.isMustUpdate() ? false : true);
            if(! dialog.isShowing())dialog.show();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private void loadUrlApk(String url_apk){
        try {
            c.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url_apk)));
            dismissDialog();
        }
        catch (android.content.ActivityNotFoundException anfe) {
            anfe.printStackTrace();
            Toast.makeText(c, "No Browser Application Installed", Toast.LENGTH_LONG).show();
        }
    }

    public void cekVersion(){
        try {
            if(svc.isBelumNgecekVersi() && isOnLine()) new VersionChecking().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            else if(! svc.isVersiTerbaru()) showDialogUpdate();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void dismissDialog(){
        if(dialog != null && dialog.isShowing()) dialog.dismiss();
    }

    private boolean isOnLine(){
        try{
            ConnectivityManager connectivity = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null){
                NetworkInfo[] info = connectivity.getAllNetworkInfo();
                if (info != null){
                    for (int i = 0; i < info.length; i++){
                        if (info[i].getState() == NetworkInfo.State.CONNECTED)
                        {
                            return true;
                        }
                    }
                }
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }

        return false;
    }

    private class VersionChecking extends AsyncTask<String, String, String>{

        private JSONObject j;

        protected void onPreExecute(){
            super.onPreExecute();
            j = null;
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                URL url = new URL(url_version_checker+"?mobile_version="+getAppVersion());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.connect();

                int resCode = conn.getResponseCode();
                InputStream in = null;
                if (resCode == HttpURLConnection.HTTP_OK) {
                    in = conn.getInputStream();
                }

                BufferedReader reader = new BufferedReader(new InputStreamReader(in, "utf-8"), 8);
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                in.close();

                String json = sb.toString();
                Log.d(" aaa ", "aaa = "+json);
                j = new JSONObject(json);

            }
            catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String x){
            super.onPostExecute(x);
            if (j != null) {
                try {
                    if (j.optInt("status_code") == 1) {
                        JSONObject status_app = j.getJSONObject("status_app");
                        boolean is_terbaru = status_app.optBoolean("is_terbaru");
                        boolean hatus_update = status_app.optBoolean("harus_update");
                        String url_update = status_app.optString("url_update");

                        svc.setIsVersiTerbaru(is_terbaru);
                        svc.setMustUpdate(hatus_update);
                        svc.setUrlUpdate(url_update);
                        svc.setIsSudahNgeCekVersi(true);

                        if (!svc.isVersiTerbaru()) showDialogUpdate();
                    }
                }
                catch(Exception e){
                    e.printStackTrace();
                }
            }
        }

    }

    private String getAppVersion(){
        String versi = "";
        PackageManager manager = c.getPackageManager();
        try {
            PackageInfo info = manager.getPackageInfo(c.getPackageName(), 0);
            versi = info.versionName;
        }
        catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versi;
    }

}