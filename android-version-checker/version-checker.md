# README #

Ini adalah library untuk mengecek versi terbaru yang ada di server dengan yg sedang berjalan di mobile.

Jika versi aplikasi di mobile sama dengan yang di server maka tidak terjadi apa2

Jika versi aplikasi di mobile berbeda dengan yg di server maka ada 2 aksi
- jika mandatory maka aplikasi di mobile WAJIB di update
- jika non-mandatory, maka aplikasi di mobile BOLEH tidak di update

Ada response url_update di server yg menunjukkan url untuk update aplikasi (playstore / http://abc.com)

Usahakan struktur response api [version_checking.php](php) tetap seperti itu

### Persiapan ###

* Download file library untuk [android](android) dan [php](php)
* Download file [contoh](example) agar mudah memahami


### Cara instalasi ###

- Ubah package di file [android](android) sesuai dengan package aplikasimu
- Di manifest aplikasi tambahkan permission
```
#!xml
	<uses-permission android:name="android.permission.INTERNET" /> 
	<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
	<uses-permission android:name="android.permission.ACCESS_WIFI_STATE"/>
```

- Di activitymu yg awal (misal splash screen)di oncreate tambahkan script
```
#!java
	SessionVersionChecker svn = new SessionVersionChecker(this);
	svn.setIsSudahNgeCekVersi(false);
```

- override method onResume dan onDestroy di semua activitymu, dengan menambahkan script

```
#!java
	private Updater u;
	public static final String URL_VERSION_CHECKER = "http://10.0.2.2/lat-gcm/version_checking.php";  //localhost untuk android

    public void onResume() {
        super.onResume();

        if (u == null) {
            u = new Updater(this);
            u.setUrlApiVersionChecker(URL_VERSION_CHECKER);   //set url api untuk ngecek versi app yg ada diserver
        }
        u.cekVersion();   //lalu ngecek
    }

    public void onDestroy() {
        super.onDestroy();
        if (u != null) u.dismissDialog();   //jika activity nutup maka dialog ditutup
    }
```
- taruh file [php](php) di server mu
- lalu untuk mengubah versi yg terbaru di file [version_checking.php](php/version_checking.php) di bagian "define" bisa kamu ubah sesuka hati

- Have Fun