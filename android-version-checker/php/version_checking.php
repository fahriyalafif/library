<?php
define("LATEST_VERSION", 2);
header('Content-Type: application/json');

$mobile_version = empty($_GET["mobile_version"]) ? "" : $_GET["mobile_version"];

$data = array();
if(empty($mobile_version))
    $data = array("status_code" => 0, "status_message" => "field mobile_version kosong");
else if($mobile_version == LATEST_VERSION){
    $status_app_terbaru = array("is_terbaru" => true, 
		"harus_update" => false, 
		"url_update" => '',
		"message" => "versi di mobile SUDAH yang terbaru");
    
	$data = array("status_code" => 1,
        "status_app" => $status_app_terbaru);
}
else {
    $harus_update = false;   //false berarti non mandatory, true berarti mandatory
    $url_update = "http://bla";   //kalo playstore nanti buka playstore, kalo http://blablbalba nanti update dari url itu
    
    $status_app_terbaru = array("is_terbaru" => false, 
		"harus_update" => $harus_update, 
		"url_update" => $url_update,
		"message" => "versi di mobile BUKAN yang terbaru");
    
	$data = array("status_code" => 1, 
        "status_app" => $status_app_terbaru);
}
echo json_encode($data);