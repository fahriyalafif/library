package id.co.firzil.cobalibrary;

import android.app.Activity;
import android.os.Bundle;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);   //mau layout apa aja terserah

        SessionVersionChecker svn = new SessionVersionChecker(this);
        svn.setIsSudahNgeCekVersi(false);   //saat membuka halaman awal diset belum ngecek ke server
    }

    private Updater u;
    public static final String URL_VERSION_CHECKER = "http://10.0.2.2/lat-gcm/version_checking.php";  //localhost untuk android

    public void onResume() {
        super.onResume();

        if (u == null) {
            u = new Updater(this);
            u.setUrlApiVersionChecker(URL_VERSION_CHECKER);   //set url api untuk ngecek versi app yg ada diserver
        }
        u.cekVersion();   //lalu ngecek
    }

    public void onDestroy() {
        super.onDestroy();
        if (u != null) u.dismissDialog();   //jika activity nutup maka dialog ditutup
    }

}
