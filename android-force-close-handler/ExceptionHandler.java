package id.co.firzil.protelindott.kelas;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;

import java.io.PrintWriter;
import java.io.StringWriter;

import id.co.firzil.protelindott.activity.ForceCloseActivity;

public class ExceptionHandler implements
		java.lang.Thread.UncaughtExceptionHandler {
	private final Activity myContext;

	public ExceptionHandler(Activity context) {
		myContext = context;
	}

	public void uncaughtException(Thread thread, Throwable exception) {
		Intent intent = new Intent(myContext, ForceCloseActivity.class);
		intent.putExtra(ForceCloseActivity.ERROR_MSG, ErrorWriter.getError(exception));
		myContext.startActivity(intent);

		android.os.Process.killProcess(android.os.Process.myPid());
		System.exit(10);
	}

}