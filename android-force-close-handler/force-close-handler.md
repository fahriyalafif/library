# README #

Force Close Handler
Library untuk menangani aplikasi yang mengalami crash (force close)
Saat terjadi FC, app akan membuka halaman yang berisi info penyebab FC dan HP yang digunakan


### Persiapan ###

Download file berikut:
* [ErrorWriter.java](ErrorWriter.java)
* [ExceptionHandler.java](ExceptionHandler.java)
* [ForceCloseActivity.java](ForceCloseActivity.java)


### Cara instalasi ###

- Ubah package di file 3 file diatas sesuai dengan package aplikasimu
- Di manifest aplikasi tambahkan permission
```
#!xml
	<activity
            android:windowSoftInputMode="adjustPan"
            android:name=".<package_name>.ForceCloseActivity"
            android:screenOrientation="portrait"
            android:configChanges="orientation|keyboard|keyboardHidden|screenSize" />
```

- Di activitymu di dalam method onCreate tambahkan script
```
#!java
	Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
```

- Enjoy

